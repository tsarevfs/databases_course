#1
SELECT Students.student_name, Groups.group_name FROM Courses
INNER JOIN Marks ON Courses.course_id = Marks.course_id
INNER JOIN Students ON Students.student_id = Marks.student_id
INNER JOIN Groups ON Groups.group_id = Marks.group_id
WHERE Courses.course_name='Physics' AND Marks.mark = '60';

#2a
SELECT s1.student_name, Groups.group_name FROM Students AS s1
INNER JOIN Groups ON Groups.group_id = s1.group_id
LEFT OUTER JOIN(
SELECT s2.student_id FROM Courses
INNER JOIN Marks ON Courses.course_id = Marks.course_id
INNER JOIN Students AS s2 ON s2.student_id = Marks.student_id
INNER JOIN Groups ON Groups.group_id = Marks.group_id
WHERE Courses.course_name='Physics'
) as not_select ON s1.student_id = not_select.student_id
WHERE not_select.student_id is NULL;

#2b
SELECT s1.student_name, Groups.group_name FROM Students AS s1
INNER JOIN Groups ON Groups.group_id = s1.group_id
INNER JOIN Schedule ON Schedule.group_id = s1.group_id
INNER JOIN Courses ON Courses.course_id = Schedule.course_id
LEFT OUTER JOIN(
SELECT s2.student_id FROM Courses
INNER JOIN Marks ON Courses.course_id = Marks.course_id
INNER JOIN Students AS s2 ON s2.student_id = Marks.student_id
INNER JOIN Groups ON Groups.group_id = Marks.group_id
WHERE Courses.course_name='Physics'
) as not_select ON s1.student_id = not_select.student_id
WHERE not_select.student_id is NULL;


#3
SELECT DISTINCT Students.student_name, Groups.group_name FROM Marks
INNER JOIN Students ON Students.student_id = Marks.student_id
INNER JOIN Groups ON Groups.group_id = Marks.group_id
INNER JOIN Schedule ON Marks.course_id = Schedule.course_id AND Marks.group_id = Schedule.group_id
INNER JOIN Lecturers ON Lecturers.lecturer_id = Schedule.lecturer_id
WHERE Lecturers.lecturer_name = 'Petr';

#4
SELECT s1.student_name, Groups.group_name FROM Students AS s1
INNER JOIN Groups ON Groups.group_id = s1.group_id
LEFT OUTER JOIN(
SELECT DISTINCT s2.student_id FROM Marks
INNER JOIN Students AS s2 ON s2.student_id = Marks.student_id
INNER JOIN Groups ON Groups.group_id = Marks.group_id
INNER JOIN Schedule ON Marks.course_id = Schedule.course_id AND Marks.group_id = Schedule.group_id
INNER JOIN Lecturers ON Lecturers.lecturer_id = Schedule.lecturer_id
WHERE Lecturers.lecturer_name = 'Petr'
) as not_select ON s1.student_id = not_select.student_id
WHERE not_select.student_id is NULL;

#6
SELECT Students.student_name, Courses.course_name FROM Students
INNER JOIN Schedule ON Schedule.group_id = Students.group_id
INNER JOIN Courses ON Courses.course_id = Schedule.course_id
;

#7
SELECT Students.student_name, Groups.group_name FROM Students
INNER JOIN Groups ON Groups.group_id = Students.group_id
INNER JOIN Schedule ON Schedule.group_id = Students.group_id
INNER JOIN Lecturers ON Lecturers.lecturer_id = Schedule.lecturer_id
WHERE Lecturers.lecturer_name = 'Petr'
;
