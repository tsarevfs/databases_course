-- -----------------------------------------------------
-- Table Groups
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Groups (
  group_id INT NOT NULL ,
  group_name VARCHAR(6) NOT NULL ,
  PRIMARY KEY (group_id) );
 
 
-- -----------------------------------------------------
-- Table Students
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Students (
  student_id INT NOT NULL ,
  student_name VARCHAR(50) NOT NULL ,
  group_id INT NOT NULL ,
  PRIMARY KEY (student_id) ,
  CONSTRAINT students_fk_group_id
    FOREIGN KEY (group_id )
    REFERENCES Groups (group_id ) );
 
 
-- -----------------------------------------------------
-- Table Courses
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Courses (
  course_id INT NOT NULL ,
  course_name VARCHAR(50) NOT NULL ,
  PRIMARY KEY (course_id) );
 
 
-- -----------------------------------------------------
-- Table Lecturers
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Lecturers (
  lecturer_id INT NOT NULL ,
  lecturer_name VARCHAR(50) NOT NULL ,
  PRIMARY KEY (lecturer_id) );
 
 
-- -----------------------------------------------------
-- Table Schedule
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Schedule (
  lecturer_id INT NOT NULL ,
  group_id INT NOT NULL ,
  course_id INT NOT NULL ,
  PRIMARY KEY (group_id, course_id) ,
  CONSTRAINT schedule_fk_lecturer_id
    FOREIGN KEY (lecturer_id )
    REFERENCES Lecturers (lecturer_id ),
  CONSTRAINT schedule_fk_course_id
    FOREIGN KEY (course_id )
    REFERENCES Courses (course_id ),
  CONSTRAINT schedule_fk_group_id
    FOREIGN KEY (group_id )
    REFERENCES Groups (group_id ) );
 
 
-- -----------------------------------------------------
-- Table Marks
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Marks (
  course_id INT NOT NULL ,
  student_id INT NOT NULL ,
  group_id INT NOT NULL ,
  mark DECIMAL(3) NOT NULL ,
  CONSTRAINT marks_fk_student_id
    FOREIGN KEY (student_id )
    REFERENCES Students (student_id ),
  CONSTRAINT marks_fk_group_id_students
    FOREIGN KEY (group_id )
    REFERENCES Students (group_id ),
  CONSTRAINT marks_fk_group_id_schedule
    FOREIGN KEY (group_id )
    REFERENCES Schedule (group_id ),
  CONSTRAINT marks_fk_course_id
    FOREIGN KEY (course_id )
    REFERENCES Schedule (course_id ) );
 
-- -----------------------------------------------------
-- Data for table Groups
-- -----------------------------------------------------
INSERT INTO Groups (group_id, group_name) VALUES
(1, '4538'),
(2, '4539');
 
-- -----------------------------------------------------
-- Data for table Students
-- -----------------------------------------------------
INSERT INTO Students (student_id, student_name, group_id) VALUES
(1, 'Oleg', 1),
(2, 'Igor', 2);
 
-- -----------------------------------------------------
-- Data for table Courses
-- -----------------------------------------------------
INSERT INTO Courses (course_id, course_name) VALUES
(1, 'Physics'),
(2, 'Maths');
 
-- -----------------------------------------------------
-- Data for table Lecturers
-- -----------------------------------------------------
INSERT INTO Lecturers (lecturer_id, lecturer_name) VALUES
(1, 'Petr'),
(2, 'Ivan');
 
-- -----------------------------------------------------
-- Data for table Schedule
-- -----------------------------------------------------
INSERT INTO Schedule (lecturer_id, group_id, course_id) VALUES 
(1, 1, 1),
(2, 2, 2);
 
-- -----------------------------------------------------
-- Data for table Marks
-- -----------------------------------------------------
INSERT INTO Marks (course_id, student_id, group_id, mark) VALUES
(1, 1, 1, 60),
(2, 2, 2, 70);
