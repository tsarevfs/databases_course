CREATE TABLE IF NOT EXISTS AnimalTypes (
	type_id SERIAL PRIMARY KEY,
	type_name    varchar(50) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS Animals (
   animal_id   SERIAL PRIMARY KEY,
   animal_name        varchar(50),
   type_id     integer NOT NULL,
   FOREIGN KEY (type_id) REFERENCES AnimalTypes(type_id)
);

CREATE TYPE resourse_type_t AS ENUM ('feed', 'product');

CREATE TABLE IF NOT EXISTS Resourses(
   resourse_id     SERIAL PRIMARY KEY,
   resourse_name   varchar(50) NOT NULL UNIQUE,
   resourse_type   resourse_type_t NOT NULL,
   resourse_amount float DEFAULT 0.0 NOT NULL,
   units           varchar(50)
);

CREATE TABLE IF NOT EXISTS Feed (
   food_id    integer PRIMARY KEY,
   FOREIGN KEY (food_id) REFERENCES Resourses(resourse_id)
);

CREATE TABLE IF NOT EXISTS Products (
   product_id integer PRIMARY KEY,
   FOREIGN KEY (product_id) REFERENCES Resourses(resourse_id)
);

CREATE TABLE IF NOT EXISTS AnimalTypeFeed (
   type_id integer NOT NULL,
   food_id integer NOT NULL,
   FOREIGN KEY (type_id) REFERENCES AnimalTypes(type_id),
   FOREIGN KEY (food_id) REFERENCES Feed(food_id)
);

CREATE TABLE IF NOT EXISTS Companies (
   company_id   SERIAL PRIMARY KEY,
   company_name varchar(255) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS Accounts (
   account_id     SERIAL PRIMARY KEY,
   account_number varchar(50) UNIQUE NOT NULL,
   company_id     integer NOT NULL,
   FOREIGN KEY (company_id) REFERENCES Companies(company_id)
);

CREATE TABLE IF NOT EXISTS Transactions (
   tr_id       SERIAL PRIMARY KEY,
   tr_size     money NOT NULL,
   from_acc_id integer NOT NULL,
   to_acc_id   integer NOT NULL,
   tr_date     timestamp NOT NULL,
   FOREIGN KEY (from_acc_id) REFERENCES Accounts(account_id),
   FOREIGN KEY (to_acc_id) REFERENCES Accounts(account_id)
);

CREATE TYPE deal_type_t AS ENUM ('sell', 'buy', 'service');

CREATE TABLE Deals (
   deal_id        SERIAL PRIMARY KEY,
   deal_type      deal_type_t NOT NULL,
   tr_id          integer NOT NULL,
   deal_date      date NOT NULL,
   FOREIGN KEY (tr_id) REFERENCES Transactions(tr_id)
);

CREATE TABLE IF NOT EXISTS ResourseDeals (
   resourse_id integer NOT NULL,
   deal_amount integer NOT NULL,
   deal_id     integer NOT NULL,
   FOREIGN KEY (resourse_id) REFERENCES Resourses(resourse_id),
   FOREIGN KEY (deal_id) REFERENCES Deals(deal_id)
);

CREATE TABLE IF NOT EXISTS AnimalDeals (
   animal_id integer NOT NULL,   
   deal_id   integer NOT NULL,
   FOREIGN KEY (animal_id) REFERENCES Animals(animal_id),
   FOREIGN KEY (deal_id) REFERENCES Deals(deal_id)
);


