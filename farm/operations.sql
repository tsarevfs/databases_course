CREATE OR REPLACE FUNCTION  buy_animal (
                                       animal_type_       varchar(50),
                                       animal_name_       varchar(50),
                                       my_acc_number_     varchar(50),
                                       seller_acc_number_ varchar(50),
                                       price_             money,
                                       tr_date_           timestamp
                                       ) RETURNS void AS
$buy_animal$
    DECLARE 
        tr_id_         integer;
        animal_id_     integer;
        deal_id_       integer;
        my_acc_id_     integer;
        seller_acc_id_ integer;
    BEGIN
      SELECT account_id FROM Accounts WHERE
         account_number = my_acc_number_ 
         INTO my_acc_id_;

      SELECT account_id FROM Accounts WHERE
         account_number = seller_acc_number_ 
         INTO seller_acc_id_;
      
      WITH res AS (
         INSERT INTO Transactions (tr_size, from_acc_id, to_acc_id, tr_date) VALUES
            (price_, my_acc_id_, seller_acc_id_, tr_date_)
            RETURNING tr_id
      ) SELECT * FROM res INTO tr_id_;
      
      WITH res AS (
         INSERT INTO Animals (animal_name, type_id)
         SELECT animal_name_, type_id FROM
            AnimalTypes WHERE 
            type_name = animal_type_
            RETURNING animal_id 
      ) SELECT * FROM res INTO animal_id_;

      WITH res AS (
         INSERT INTO Deals (deal_type, tr_id, deal_date) VALUES
            ('buy', tr_id_, tr_date_::date)
            RETURNING deal_id
      ) SELECT * FROM res INTO deal_id_;

      INSERT INTO AnimalDeals VALUES
         (animal_id_, deal_id_);
    END;
$buy_animal$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION on_resourse_add() RETURNS trigger AS
$on_resourse_add$
BEGIN
   IF NEW.resourse_type = 'feed' THEN
      INSERT INTO Feed VALUES
      (NEW.resourse_id);
   END IF;
   IF NEW.resourse_type = 'product' THEN
      INSERT INTO Products VALUES
      (NEW.resourse_id);
   END IF;

   RETURN NEW;
END;
$on_resourse_add$ LANGUAGE plpgsql;

CREATE TRIGGER resourse_add_trigger AFTER INSERT ON Resourses
    FOR EACH ROW EXECUTE PROCEDURE on_resourse_add();


CREATE OR REPLACE FUNCTION buy_feed(
                                    food_name_ varchar(50),
                                    amount_     float,
                                    my_acc_number_     varchar(50),
                                    seller_acc_number_ varchar(50),
                                    price_             money,
                                    tr_date_           timestamp
                                    ) RETURNS boolean AS
$buy_feed$
DECLARE 
        tr_id_         integer;
        deal_id_       integer;
        resourse_id_   integer;
        my_acc_id_     integer;
        seller_acc_id_ integer;
BEGIN
   SELECT account_id FROM Accounts WHERE
      account_number = my_acc_number_ 
      INTO my_acc_id_;

   SELECT account_id FROM Accounts WHERE
      account_number = seller_acc_number_ 
      INTO seller_acc_id_;

   SELECT food_id FROM Feed INNER JOIN Resourses ON Feed.food_id = Resourses.resourse_id WHERE
   resourse_name = food_name_
   INTO resourse_id_;

   IF NOT EXISTS (SELECT * FROM
      Animals NATURAL JOIN AnimalTypes 
      NATURAL JOIN AnimalTypeFeed INNER JOIN Resourses ON 
      Resourses.resourse_id = AnimalTypeFeed.food_id WHERE 
      Resourses.resourse_id = resourse_id_)
   THEN
      RETURN false;
   END IF;

   WITH res AS (
      INSERT INTO Transactions (tr_size, from_acc_id, to_acc_id, tr_date) VALUES
         (price_, my_acc_id_, seller_acc_id_, tr_date_)
         RETURNING tr_id
   ) SELECT * FROM res INTO tr_id_;

   WITH res AS (
      INSERT INTO Deals (deal_type, tr_id, deal_date) VALUES
         ('buy', tr_id_, tr_date_::date)
         RETURNING deal_id
   ) SELECT * FROM res INTO deal_id_;

   SELECT food_id FROM Feed INNER JOIN Resourses ON Feed.food_id = Resourses.resourse_id WHERE
   resourse_name = food_name_
   INTO resourse_id_;

   INSERT INTO ResourseDeals VALUES
      (resourse_id_, amount_, deal_id_);

   UPDATE Resourses SET resourse_amount = resourse_amount + amount_ WHERE resourse_id = resourse_id_; 
   RETURN true;
END;
$buy_feed$ LANGUAGE plpgsql;
