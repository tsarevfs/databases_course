INSERT INTO AnimalTypes (type_name) VALUES
('cow'),
('chicken'),
('pig');

INSERT INTO Resourses (resourse_name, resourse_type) VALUES
('hay', 'feed'),
('feed', 'feed'),
('humans', 'feed'),
('wheat', 'feed'),
('potato', 'feed'),
('milk', 'product'),
('meat', 'product'),
('wool', 'product');

INSERT INTO AnimalTypeFeed
SELECT type_id, food_id FROM AnimalTypes, (Feed INNER JOIN Resourses ON Feed.food_id = Resourses.resourse_id) WHERE
   type_name = 'cow' and resourse_name = 'hay';
INSERT INTO AnimalTypeFeed
SELECT type_id, food_id FROM AnimalTypes, (Feed INNER JOIN Resourses ON Feed.food_id = Resourses.resourse_id) WHERE
   type_name = 'cow' and resourse_name = 'feed';
INSERT INTO AnimalTypeFeed
SELECT type_id, food_id FROM AnimalTypes, (Feed INNER JOIN Resourses ON Feed.food_id = Resourses.resourse_id) WHERE
   type_name = 'pig' and resourse_name = 'feed';
INSERT INTO AnimalTypeFeed
SELECT type_id, food_id FROM AnimalTypes, (Feed INNER JOIN Resourses ON Feed.food_id = Resourses.resourse_id) WHERE
   type_name = 'pig' and resourse_name = 'humans';
INSERT INTO AnimalTypeFeed
SELECT type_id, food_id FROM AnimalTypes, (Feed INNER JOIN Resourses ON Feed.food_id = Resourses.resourse_id) WHERE
   type_name = 'chicken' and resourse_name = 'food';
INSERT INTO AnimalTypeFeed
SELECT type_id, food_id FROM AnimalTypes, (Feed INNER JOIN Resourses ON Feed.food_id = Resourses.resourse_id) WHERE
   type_name = 'chicken' and resourse_name = 'wheat';

INSERT INTO Companies (company_name) VALUES
('ZAO Roga i Kapyta'),
('OAO Sopranos'),
('self');

INSERT INTO Accounts (account_number, company_id)
SELECT '2223222', company_id FROM Companies WHERE
   company_name = 'self'; 
INSERT INTO Accounts (account_number, company_id)
SELECT '111', company_id FROM Companies WHERE
   company_name = 'ZAO Roga i Kapyta'; 
INSERT INTO Accounts (account_number, company_id)
SELECT '222', company_id FROM Companies WHERE
   company_name = 'ZAO Roga i Kapyta'; 
INSERT INTO Accounts (account_number, company_id)
SELECT '148', company_id FROM Companies WHERE
   company_name = 'OAO Sopranos'; 

SELECT buy_animal('pig', NULL, '2223222', '111', 999::money, now()::timestamp);
SELECT buy_animal('cow', 'Burenka', '2223222', '222', 1999::money, now()::timestamp);
SELECT buy_animal('chicken', NULL, '2223222', '222', 99::money, now()::timestamp);
SELECT buy_animal('chicken', NULL, '2223222', '222', 99::money, now()::timestamp);
SELECT buy_animal('chicken', NULL, '2223222', '222', 99::money, now()::timestamp);
SELECT buy_animal('chicken', NULL, '2223222', '222', 99::money, now()::timestamp);

SELECT buy_feed('humans', 10.0, '2223222', '148', 99::money, now()::timestamp);
SELECT buy_feed('hay', 100.0, '2223222', '148', 555::money, now()::timestamp);
