-- Что едят животные
SELECT type_name, resourse_name AS food_name FROM
   (Resourses INNER JOIN AnimalTypeFeed ON Resourses.resourse_id = AnimalTypeFeed.food_id)
   NATURAL JOIN Animaltypes;

-- Каких кормов нет, для присутствующих животных
SELECT DISTINCT resourse_name AS food_name FROM
   (Resourses INNER JOIN AnimalTypeFeed ON Resourses.resourse_id = AnimalTypeFeed.food_id)
   NATURAL JOIN (Animaltypes NATURAL JOIN Animals) 
   WHERE resourse_amount < 0.01;

